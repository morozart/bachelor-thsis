import torch
from torchvision import models
from torch import nn
import numpy as np
import time
import random
from numpy import asarray
import copy
from create_batches_multi_input import BATCH_LOADER
from datAugMultiInput import multi_input_model_augmentation
from Statistics_computation import *
from numpy import savetxt
PATH_PROJECT = "/home.stud/morozart/"


def save_history_multi_input_model(model_name, train_loss_history = None, val_loss_history = None, Spearman_history = None, Pearson_history = None):
    """
    Function stores training loss, validation loss, Spearman rank corr. coeff, Pearson rank corr. coeff histories obtained during the training.
    :param model_name:
    :type string:
    :param train_loss_history:
    :type ndarray:
    :param val_loss_history:
    :type ndarray:
    :param Spearman_history:
    :type ndarray:
    :param Pearson_history:
    :type ndarray:
    :return: None
    :type None:
    """
    savetxt("training_loss_history_{}.csv".format(model_name), train_loss_history, delimiter=",")
    savetxt("validation_loss_history_{}.csv".format(model_name), val_loss_history, delimiter=",")
    savetxt("Spearman_coef_history_{}.csv".format(model_name), Spearman_history, delimiter=",")
    savetxt("Pearson_coef_history_{}.csv".format(model_name), Pearson_history, delimiter=",")

def test_model(model, criterion = None, dataloaders = None, optimizer = None, concat_images = False):
    """
    Function implements testing phase for CNN, computes loss, Spearman and Pearson corr. coeffs on testing data
    :param model, criterion = None, dataloaders = None, optimizer = None, concat_images = False
    :return None:
    """
    model.eval()
    all_outs = np.array([])
    all_labels = np.array([])
    total_loss = 0
    with torch.no_grad():
        for index, batch in enumerate(dataloaders["test"]):
            inputs_segmentation = torch.from_numpy(batch["data"][0])/255 # normalize segmentation img
            inputs_ultrasound = torch.from_numpy(batch["data"][1])/255 # normalize ultrasound img

            labels = torch.from_numpy(batch["labels"]).to(DEVICE)/2  # normalize label (for Homogeneity /2, for Echogenicity /4, for Degree of stenosis /100)

            optimizer.zero_grad()

            if concat_images: # for model number 1 only
                inputs_concatenated = torch.cat((inputs_segmentation, inputs_ultrasound), dim=3).permute(0, 3, 1, 2)
                outputs = model(inputs_concatenated.float().to(DEVICE))
            if not concat_images:
                outputs = model(inputs_segmentation.permute(0, 3, 1, 2).float().to(DEVICE), inputs_ultrasound.permute(0, 3, 1, 2).float().to(DEVICE))

            all_labels = np.append(all_labels, labels.cpu().numpy())
            all_outs = np.append(all_outs, outputs.cpu().detach().numpy())
            loss = criterion(outputs, labels.float())
            total_loss += loss.item()

            torch.cuda.empty_cache()
    PearsonTest = computePearson(all_labels, all_outs)
    SpearmanTest = computeSpearman(all_labels, all_outs)

    print("Pearson correlation: ", PearsonTest, "Spearman correlation: ", SpearmanTest)


def train_multi_input_model(model, model_name, dataloaders, criterion, optimizer, num_epochs=100):
    """
      Function implements the main training process for models with two input images
      :param model, model_name, dataloaders, criterion, optimizer, num_epochs:
      :return None:
    """
    since = time.time()

    training_loss_history = []
    val_loss_history = []
    Pearson_history = []
    Spearman_history = []

    best_Pearson = 0
    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs))
        print('-' * 30)

        for phase in ['train', 'val']:
            epoch_loss = 0.0
            if phase == 'train':
                model.train()
                with torch.enable_grad():
                    for index, batch in enumerate(dataloaders[phase]):
                        input_segmentation = batch["data"][0]/255  # normalize segmentation img
                        input_ultrasound = batch["data"][1]/255 # normalize ultrasound img
                        augmented_input_ultrasound = copy.deepcopy(input_ultrasound)
                        augmented_input_segmentation = copy.deepcopy(input_segmentation)

                        apply_augment = random.random()
                        if apply_augment > 0.75:   # 25 percent of application augmentation strategy
                            augmented_input_ultrasound, augmented_input_segmentation = multi_input_model_augmentation(augmented_input_ultrasound[0], augmented_input_segmentation[0])
                            augmented_input_ultrasound = np.array([augmented_input_ultrasound])
                            augmented_input_segmentation = np.array([augmented_input_segmentation])

                        augmented_input_segmentation = torch.from_numpy(augmented_input_segmentation)
                        augmented_input_ultrasound = torch.from_numpy(augmented_input_ultrasound)

                        labels = torch.from_numpy(batch["labels"]).to(DEVICE)/2  # normalize label (for Homogeneity /2, for Echogenicity /4, for Degree of stenosis /100)
                        optimizer.zero_grad()

                        if MULTIPLE_INPUT_MODEL != 0:
                            outputs = model(augmented_input_segmentation.permute(0, 3, 1, 2).float().to(DEVICE), augmented_input_ultrasound.permute(0, 3, 1,2).float().to(DEVICE))
                        else:
                            input_to_model = torch.cat((augmented_input_segmentation, augmented_input_ultrasound), dim=3).permute(0, 3,1,2)
                            outputs = model(input_to_model.float().to(DEVICE))

                        loss = criterion(outputs, labels.float())
                        loss.backward()
                        optimizer.step()

                        epoch_loss += loss.item()
                        torch.cuda.empty_cache()
                training_loss_history.append(epoch_loss)
                print('{} Loss: {:.4f}'.format(phase, epoch_loss))

            elif phase == "val":
                model.eval()
                whole_outs = np.array([])
                whole_labels = np.array([])
                with torch.no_grad():
                    for index, batch in enumerate(dataloaders[phase]):
                        optimizer.zero_grad()
                        input_segmentation = torch.from_numpy(batch["data"][0])/255 # normalize segmentation img
                        input_ultrasound = torch.from_numpy(batch["data"][1])/255 # normalize ultrasound img

                        labels = torch.from_numpy(batch["labels"]).to(DEVICE)/2 # normalize label (for Homogeneity /2, for Echogenicity /4, for Degree of stenosis /100)
                        if MULTIPLE_INPUT_MODEL != 0:
                            outputs = model(input_segmentation.permute(0, 3, 1, 2).float().to(DEVICE), input_ultrasound.permute(0, 3, 1, 2).float().to(DEVICE))
                        else:
                            input_to_model = torch.cat((input_segmentation, input_ultrasound), dim=3).permute(0, 3, 1, 2)
                            outputs = model(input_to_model.float().to(DEVICE))

                        whole_labels = np.append(whole_labels, labels.cpu().numpy())
                        whole_outs = np.append(whole_outs, outputs.cpu().numpy())
                        loss = criterion(outputs, labels.float())

                        epoch_loss += loss.item()
                        torch.cuda.empty_cache()

                val_loss_history.append(epoch_loss)
                Spearman_corr_coef = computeSpearman(true_labels=whole_labels, predicted_labels=whole_outs)
                Pearson_corr_coef = computePearson(true_labels=whole_labels, predicted_labels=whole_outs)
                Pearson_history.append(Pearson_corr_coef)
                Spearman_history.append(Spearman_corr_coef)

                if Pearson_corr_coef > best_Pearson:
                    best_Pearson = Pearson_corr_coef
                    print("Saving best model weights!")
                    torch.save(model.state_dict(), PATH_PROJECT + "{}.pt".format(model_name))
                print('{} Loss: {:.4f}, Spearman {}, Pearson {}'.format(phase, epoch_loss, Spearman_corr_coef, Pearson_corr_coef))

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))

    train_loss_history = asarray([training_loss_history])
    val_loss_history = asarray([val_loss_history])
    Spearman_history = asarray([Spearman_history])
    Pearson_history = asarray([Pearson_history])

    save_history_multi_input_model(model_name=model_name, train_loss_history=train_loss_history, val_loss_history = val_loss_history, Spearman_history=Spearman_history,
                                Pearson_history = Pearson_history)


class ConcatenatedFCModel(nn.Module): # Model 3
    """
    Class ConcatenatedFCModel creates the second variation of model accepts two input images
    """
    def __init__(self, pretrained):
        super().__init__()
        self.resnet_model = models.resnet34(pretrained=pretrained)
        self.model1 = nn.Sequential(*list(self.resnet_model.children())[:-2])
        self.model2 = nn.Sequential(*list(self.resnet_model.children())[:-2])
        self.fully_connected_layer = nn.Sequential(nn.Flatten(),
                                                   nn.Linear(65536, 2048, bias=True),
                                                   nn.ReLU(),
                                                   nn.Linear(2048, 1024, bias=True),
                                                   nn.ReLU(),
                                                   nn.Linear(1024, 1, bias=True))

        if pretrained is False:
            for model_part in [self.model1, self.model2, self.fully_connected_layer]:
                for lay in model_part.modules():
                    if type(lay) in [nn.Conv2d, nn.Linear]:
                        torch.nn.init.xavier_uniform(lay.weight)

    def forward(self, x1, x2):
        x1 = self.model1(x1)
        x2 = self.model2(x2)
        x3 = torch.cat((x1, x2), dim=1)
        out = self.fully_connected_layer(x3)
        return out


class ConcatenatedMiddleModel(nn.Module): # Model 2
    """
    Class ConcatenatedMiddleModel creates the second variation of model accepts two input images
    """
    def __init__(self, pretrained):
        super().__init__()
        self.resnet_model = models.resnet34(pretrained=pretrained)
        self.model1 = nn.Sequential(*list(self.resnet_model.children())[:6])
        self.model2 = nn.Sequential(*list(self.resnet_model.children())[:6])
        self.model3 = nn.Sequential(*list(self.resnet_model.children())[6][1:])
        self.model4 = nn.Sequential(*list(self.resnet_model.children())[7:-2])
        self.fully_connected_layer = nn.Sequential(nn.Flatten(),
                                                    nn.Linear(131072, 2048, bias=True),
                                                   nn.ReLU(),
                                                   nn.Linear(2048, 1024, bias=True),
                                                   nn.ReLU(),
                                                   nn.Linear(1024, 1, bias=True))

        if pretrained is False:
            for model_part in [self.model1, self.model2, self.model3, self.model4, self.fully_connected_layer]:
                for lay in model_part.modules():
                    if type(lay) in [nn.Conv2d, nn.Linear]:
                        torch.nn.init.xavier_uniform(lay.weight)

    def forward(self, x1, x2):
        x1 = self.model1(x1)
        x2 = self.model2(x2)
        x3 = torch.cat((x1, x2), dim=1)
        x3 = self.model3(x3)
        x4 = self.model4(x3)
        out = self.fully_connected_layer(x4)
        return out


class ConcatenatedInputModel(nn.Module): # Model 1
    """
    Class ConcatenatedInputModel creates the first variation of model accepts two input images
    """
    def __init__(self, pretrained):
        super().__init__()
        self.resnet_model = models.resnet34(pretrained=pretrained)
        self.input_conv_layer = nn.Conv2d(in_channels=6, out_channels=64, kernel_size=(7,7), stride=(2,2), padding=(3,3), bias = False)
        self.model1 = nn.Sequential(*list(self.resnet_model.children())[1:-2])
        self.fully_connected_layer = nn.Sequential(nn.Flatten(),
                                                   nn.Linear(32768, 2048, bias=True),
                                                   nn.ReLU(),
                                                   nn.Linear(2048, 1024, bias=True),
                                                   nn.ReLU(),
                                                   nn.Linear(1024, 1, bias=True))

        if pretrained is False:
            for model_part in [self.input_conv_layer, self.model1, self.fully_connected_layer]:
                for lay in model_part.modules():
                    if type(lay) in [nn.Conv2d, nn.Linear]:
                        torch.nn.init.xavier_uniform(lay.weight)

    def forward(self, x):
        x = self.input_conv_layer(x)
        x = self.model1(x)
        out = self.fully_connected_layer(x)
        return out


if __name__ == "__main__":

    DEVICE_NUMBER = 0
    if torch.cuda.is_available():
        print("CUDA IS AVAILABLE, DEVICE NUMBER {}".format(DEVICE_NUMBER) )
        DEVICE = torch.device(DEVICE_NUMBER)
    else:
        print("NO CUDA IS AVAILABLE, TRAINING ON CPU")
        DEVICE = torch.device("cpu")

    MULTIPLE_INPUT_MODEL = 1 # 1 -> Model 1  2 -> Model 2, 3 -> Model 3
    BATCH_SIZE = 1
    WEIGH_DECAY = 0.0005
    LEARNING_RATE = 5 * (10)**(-6)
    EPOCHS_NUMBER = 250

    if MULTIPLE_INPUT_MODEL == 2:
        model_name = "CONCATENATED_MIDDLE_MODEL"
        model = ConcatenatedMiddleModel(pretrained=False)
    elif MULTIPLE_INPUT_MODEL == 1:
        model_name = "CONCATENATED_INPUT_MODEL"
        model = ConcatenatedInputModel(pretrained=False)
    elif MULTIPLE_INPUT_MODEL == 3:
        model_name = "CONCATENATED_FC_MODEL"
        model = ConcatenatedFCModel(pretrained=False)

    if torch.cuda.is_available():
        model = model.to(DEVICE)

    dataloader = BATCH_LOADER()

    loss_function = torch.nn.MSELoss()
    optimizer = torch.optim.Adam(params=model.parameters(), lr = LEARNING_RATE, weight_decay = WEIGH_DECAY)

    print("START TRAINING {}".format(model_name))

    train_multi_input_model(model, model_name=model_name, dataloaders = dataloader, criterion = loss_function, optimizer = optimizer, num_epochs = EPOCHS_NUMBER)

    test_model(model = model ,concat_images= True, dataloaders=dataloader, criterion=loss_function, optimizer = optimizer)
