import numpy as np
import csv
import pickle


def create_per_patient_dict():
	"""
	In data_with_attributes.csv not all images have the corresponding segmentation and also not for all patients experts annotation was available
	This function selects only images for which exits the segmentation and also expert annotation is  availabe
	:return: None
	"""
	file = open("data_with_attributes.csv")

	csvreader = csv.reader(file)
	patient_image_dict = dict()
	patient_echogenity_dict = dict()
	patient_side_orient = dict()

	patient_id_stable = list()
	img_name_stable = list()
	labels_stable = list()
	side_progressive = list()
	side_stable = list()
	patient_id_progressive = list()
	img_name_progressive = list()
	labels_progressive = list()

	for row in csvreader:
		if len(row[19]) > 0 and len(row[13]) > 0:
			if row[8] == "progressive":
				patient_id_stable.append(row[3])
				img_name_stable.append(row[13])
				labels_stable.append(row[20])
				side_stable.append(row[12])
			elif row[8] == "stable":
				patient_id_progressive.append(row[3])
				img_name_progressive.append(row[13])
				labels_progressive.append(row[20])
				side_progressive.append(row[12])
	assert len(img_name_stable) == len(labels_stable) and len(labels_stable) == len(patient_id_stable)
	assert len(img_name_progressive) == len(labels_progressive) and len(labels_progressive) == len(patient_id_progressive)

	progressive_set_id = set(patient_id_progressive)
	stable_set_id = set(patient_id_stable)


	counter = 0
	for patient_id in stable_set_id:
		patient_imgs = []
		labels = []
		sides = []
		for i in range(len(patient_id_stable)):
			if patient_id_stable[i] == patient_id:
				patient_imgs.append(img_name_stable[i])
				labels.append(labels_stable[i])
				sides.append(side_stable[i])
		patient_image_dict[str(counter)] = patient_imgs
		patient_echogenity_dict[str(counter)] = labels
		patient_side_orient[str(counter)] = sides
		counter += 1

	for patient_id in progressive_set_id:
		patient_imgs = []
		labels = []
		sides = []
		for i in range(len(patient_id_progressive)):
			if patient_id_progressive[i] == patient_id:
				patient_imgs.append(img_name_progressive[i])
				labels.append(labels_progressive[i])
				sides.append(side_progressive[i])
		patient_image_dict[str(counter)] = patient_imgs
		patient_echogenity_dict[str(counter)] = labels
		patient_side_orient[str(counter)] = sides
		counter += 1


	file_patient_imgname_label = open("data_with_attributes_annotation_segmentation.csv", "w")
	csvwriter = csv.writer(file_patient_imgname_label)
	csvwriter.writerow(["ID", "Path to ultrasound image", "Side", "Orientation", "Echogenicity", "Homogeneity"])
	for key in patient_image_dict.keys():
		for i in range(len(patient_image_dict[key])):
			csvwriter.writerow([key, patient_image_dict[key][i], patient_side_orient[key][i] ,patient_echogenity_dict[key][i]])

	file_patient_imgname_label.close()

create_per_patient_dict()
